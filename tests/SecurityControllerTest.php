<?php

namespace App\Tests;

use Symfony\Component\Panther\PantherTestCase;

class SecurityControllerTest extends PantherTestCase
{
    public function testLoginPage()
    {
        $client = static::createPantherClient();
        $crawler = $client->request('GET', '/');

        $this->assertSame(self::$baseUri.'/login', $client->getCurrentURL());
        $this->assertCount(1, $crawler->filter('h1'));
        $this->assertContains('Login', $crawler->filter('title')->html());
        $this->assertContains('Please sign in', $crawler->filter('h1')->text());
        $this->assertContains('I\'ve forgot my password :(', $crawler->filter('#forgot_password')->text());

        $client->clickLink('I\'ve forgot my password :(');
        $this->assertSame(self::$baseUri.'/password-forgotten', $client->getCurrentURL());

    }

    public function testLogin()
    {
        $client = static::createPantherClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/login');

        $client->submitForm('Sign in', ['email' => 'admin@meetin.com', 'password' => 'admin123']);
        $this->assertSame(self::$baseUri.'/', $client->getCurrentURL());

    }

    public function testAdminDashboard()
    {
        $client = static::createPantherClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/admin');

        $this->assertSame("Meet'in admin dashboard", $crawler->filter('#header-logo')->text());

    }

    public function testLogout()
    {
        $client = static::createPantherClient();
        $client->request('GET', '/logout');

        $this->assertSame(self::$baseUri.'/login', $client->getCurrentURL());

    }
}
