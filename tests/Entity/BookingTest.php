<?php

namespace App\Tests\Entity;


use App\Entity\Booking;
use App\Entity\Room;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class BookingTest extends TestCase
{
    private $booking;

    private $room;
    private $user;

    public function setUp()
    {
        $this->booking = new Booking();
    }

    public function testTheWholeEntityBooking(): void
    {
        $this->booking->setTitle("test booking Title");
        $this->assertSame("test booking Title", $this->booking->getTitle());

        $this->booking->setBeginAt("2018-12-17 12:32:00");
        $this->assertSame("2018-12-17 12:32:00", $this->booking->getBeginAt());

        $this->booking->setEndAt("2018-12-17 18:00:00");
        $this->assertSame("2018-12-17 18:00:00", $this->booking->getEndAt());

        $this->room = new Room();
        $this->room->setName('RoomTestBooking');
        $this->room->setCapacity(10);

        $this->booking->setRoom($this->room);
        $this->assertSame('RoomTestBooking', $this->booking->getRoom()->getName());
        $this->assertSame(10, $this->booking->getRoom()->getCapacity());

        $this->user = New User();
        $this->user->setName('Letesteur');
        $this->user->setFirstname('Fab');

        $this->booking->setUser($this->user);
        $this->assertSame('Letesteur', $this->booking->getUser()->getName());
        $this->assertSame('Fab', $this->booking->getUser()->getFirstname());

        $this->assertSame("open", $this->booking->getStatus());


        $this->assertSame("test booking Title", $this->booking->__toString());


    }

}