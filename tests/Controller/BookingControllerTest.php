<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 08/01/2019
 * Time: 17:19
 */

namespace App\Tests\Controller;


use Symfony\Component\Panther\PantherTestCase;

class BookingControllerTest extends PantherTestCase
{
    public function testNewBookingRegistration()
    {
        $client = static::createPantherClient();
        $client->followRedirects();

        $client->submitForm('Sign in', ['email' => 'admin@meetin.com', 'password' => 'admin123']);

        $client->clickLink('New Booking');
        $client->submitForm('Save', [
            'booking{title}' => 'test Panter',
            'booking{beginAt}' => '2019-01-15 15:00',
            'booking{endAt}' => '2019-01-15 17:00',
            'booking{room}' => 'room1'
        ]);
    }
}