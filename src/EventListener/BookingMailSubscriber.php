<?php
/**
 * Created by PhpStorm.
 * User: pandora
 * Date: 07/01/19
 * Time: 15:46
 */

namespace App\EventListener;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Booking;
use App\Service\MailerService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class BookingMailSubscriber implements EventSubscriberInterface
{

    private $tokenStorage;
    private $mailer;

    public function __construct(TokenStorageInterface $tokenStorage, MailerService $mailer)
    {

        $this->tokenStorage = $tokenStorage;
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents()
    {
        return [KernelEvents::VIEW => [['sendMailDetails', EventPriorities::POST_WRITE], ['sendMailUpdate', EventPriorities::POST_WRITE], ['sendMailDelete', EventPriorities::PRE_WRITE]]];
    }

    public function sendMailDetails(GetResponseForControllerResultEvent $event)
    {
        $booking = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        if (!$booking instanceof Booking || Request::METHOD_POST !== $method) {
            return;
        }

        $user = $booking->user;
        $this->mailer->sendBookingDetails($user, $booking);

    }

    public function sendMailUpdate(GetResponseForControllerResultEvent $event)
    {
        $booking = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        if (!$booking instanceof Booking || Request::METHOD_PUT !== $method) {
            return;
        }

        $user = $booking->user;
        $this->mailer->sendBookingUpdate($user, $booking);
    }

    public function sendMailDelete(GetResponseForControllerResultEvent $event)
    {
        $booking = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        if (!$booking instanceof Booking || Request::METHOD_DELETE !== $method) {
            return;
        }

        $user = $booking->user;
        $this->mailer->sendBookingDelete($user, $booking);
    }
}