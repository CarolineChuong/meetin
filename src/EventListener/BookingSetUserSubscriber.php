<?php
/**
 * Created by PhpStorm.
 * User: pandora
 * Date: 07/01/19
 * Time: 13:38
 */

namespace App\EventListener;


use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Booking;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class BookingSetUserSubscriber implements EventSubscriberInterface
{

    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {

        $this->tokenStorage = $tokenStorage;
    }

    public static function getSubscribedEvents()
    {
        return [KernelEvents::VIEW => ['setCurrentUser', EventPriorities::PRE_WRITE]];
    }

    public function setCurrentUser(GetResponseForControllerResultEvent $event)
    {
        $booking = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        if (!$booking instanceof Booking || Request::METHOD_POST !== $method) {
            return;
        }

        $user = $this->tokenStorage->getToken()->getUser();

        if (!($user instanceof User)) {
            return;
        }

        $booking->setUser($user);
        $event->setControllerResult($booking);
    }
}