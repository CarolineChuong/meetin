<?php
/**
 * Created by PhpStorm.
 * User: pandora
 * Date: 07/12/18
 * Time: 11:54
 */

namespace App\EventListener;


use App\Entity\Room;
use App\Entity\User;
use App\Service\MailerService;
use App\Service\TokenGeneratorService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class EasyAdminUserSubscriber implements EventSubscriberInterface
{

    private $passwordEncoder;
    private $tokenGenerator;
    private $mailer;

    /**
     * EasyAdminUserSubscriber constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param TokenGeneratorService $tokenGenerator
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, TokenGeneratorService $tokenGenerator, MailerService $mailer)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->tokenGenerator = $tokenGenerator;
        $this->mailer = $mailer;
    }

    /**
     * Listen to pre persist and pre update in order to encode the password
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return ['easy_admin.pre_persist' => ['encodePassword']];
    }

    /**
     * Encode the user password
     * @param GenericEvent $event
     */
    public function encodePassword(GenericEvent $event)
    {
        $entity = $event->getSubject();

        if (!($entity instanceof User)) {
            return;
        }

        $token = $this->tokenGenerator->generateToken();

        $url = $this->tokenGenerator->generateRoute($token);

        $entity->setPassword($this->passwordEncoder->encodePassword($entity, $token));
        $event['entity'] = $entity;

        $this->mailer->sendActivationLink($entity, $url);
    }

}