<?php
/**
 * Created by PhpStorm.
 * User: pandora
 * Date: 15/12/18
 * Time: 19:08
 */

namespace App\Service;

use Symfony\Component\Templating\EngineInterface;

class MailerService
{
    private $mailer;
    private $templating;

    public function __construct(\Swift_Mailer $mailer, EngineInterface $templating)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    public function sendActivationLink($user, $url)
    {
        $message = (new \Swift_Message('Welcome'))
            ->setFrom('admin@meetin.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->templating->render(
                    'emails/welcome.html.twig',[
                        'user' => $user,
                        'url' => $url
                    ]),
                'text/html'
            );
        $this->mailer->send($message);
    }

    public function sendAccountConfirmation($user)
    {
        $message = (new \Swift_Message('Account activated'))
            ->setFrom('admin@meetin.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->templating->render(
                    'emails/account-activated.html.twig',[
                    'user' => $user
                ]),
                'text/html'
            );
        $this->mailer->send($message);
    }

    public function sendNewToken($user, $url)
    {
        $message = (new \Swift_Message('New password requested'))
            ->setFrom('admin@meetin.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->templating->render(
                    'emails/new-token-password.html.twig',[
                    'user' => $user,
                    'url' => $url
                ]),
                'text/html'
            );
        $this->mailer->send($message);
    }


    public function sendBookingDetails($user, $booking)
    {
        $message = (new \Swift_Message('Booking confirmed'))
            ->setFrom('admin@meetin.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->templating->render(
                    'emails/booking-details.html.twig',[
                    'user' => $user,
                    'booking' => $booking
                ]),
                'text/html'
            );
        $this->mailer->send($message);
    }

    public function sendBookingUpdate($user, $booking)
    {
        $message = (new \Swift_Message('Booking updated'))
            ->setFrom('admin@meetin.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->templating->render(
                    'emails/booking-update.html.twig',[
                    'user' => $user,
                    'booking' => $booking
                ]),
                'text/html'
            );
        $this->mailer->send($message);
    }

    public function sendBookingDelete($user, $booking)
    {
        $message = (new \Swift_Message('Booking cancelled'))
            ->setFrom('admin@meetin.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->templating->render(
                    'emails/booking-delete.html.twig',[
                    'user' => $user,
                    'booking' => $booking
                ]),
                'text/html'
            );
        $this->mailer->send($message);
    }
}