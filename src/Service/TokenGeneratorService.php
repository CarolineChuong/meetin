<?php
/**
 * Created by PhpStorm.
 * User: pandora
 * Date: 10/12/18
 * Time: 14:58
 */

namespace App\Service;


use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class TokenGeneratorService
{
    private $router;

    public function __construct(UrlGeneratorInterface $router)
    {
        $this->router = $router;
    }

    public function generateToken()
    {
        return str_replace('/', '-', password_hash(uniqid('', true), PASSWORD_ARGON2I));
    }

    public function generateRoute($token)
    {
        return $this->router->generate('security_confirm_token', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL);
    }
}