<?php
/**
 * Created by PhpStorm.
 * User: pandora
 * Date: 08/01/19
 * Time: 12:03
 */

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class WeekdayValidator extends ConstraintValidator
{

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof Weekday) {
            throw new UnexpectedTypeException($constraint, Weekday::class);
        }

        if (!($value instanceof \DateTime)) {

            throw new UnexpectedValueException($value, 'DateTime');

        }

        if ($this->isWeekend($value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value->format('d-m-Y H:i'))
                ->addViolation();
        }
    }

    /**
     * @param \DateTime $date
     * @return mixed
     */
    function isWeekend($date) {
        return $date->format('N')>= 6;
    }
}