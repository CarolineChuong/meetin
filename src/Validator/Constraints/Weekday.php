<?php
/**
 * Created by PhpStorm.
 * User: pandora
 * Date: 08/01/19
 * Time: 11:56
 */

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Weekday extends Constraint
{

    public $message = 'You can only book from Monday to Friday. "{{ string }}" is not a weekday.';

}