<?php
/**
 * Created by PhpStorm.
 * User: pandora
 * Date: 08/01/19
 * Time: 15:21
 */

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class WorkingHour extends Constraint
{

    public $message = 'You can only book from 8AM to 7PM. "{{ string }}" is not on working hours.';

}