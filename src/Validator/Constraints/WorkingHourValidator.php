<?php
/**
 * Created by PhpStorm.
 * User: pandora
 * Date: 08/01/19
 * Time: 15:23
 */

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class WorkingHourValidator extends ConstraintValidator
{

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof WorkingHour) {
            throw new UnexpectedTypeException($constraint, WorkingHour::class);
        }

        if (!($value instanceof \DateTime)) {
            // throw this exception if your validator cannot handle the passed type so that it can be marked as invalid
            throw new UnexpectedValueException($value, 'DateTime');

        }

        if (!$this->isWorkingHour($value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value->format('H:i'))
                ->addViolation();
        }
    }

    /**
     * @param \DateTime $date
     * @return mixed
     */
    function isWorkingHour($date) {

        return $date->format('H:i')>= '08:00' && $date->format('H:i')<= '19:00';
    }
}