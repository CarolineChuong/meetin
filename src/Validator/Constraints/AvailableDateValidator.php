<?php
/**
 * Created by PhpStorm.
 * User: pandora
 * Date: 08/01/19
 * Time: 16:00
 */

namespace App\Validator\Constraints;


use App\Entity\Booking;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class AvailableDateValidator extends ConstraintValidator
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function validate($object, Constraint $constraint)
    {
        if (!$constraint instanceof AvailableDate) {
            throw new UnexpectedTypeException($constraint, AvailableDate::class);
        }

        /** Booking $object */
        $roomId = $object->getRoom()->getId();

        $beginAt = $object->getBeginAt();
        $endAt = $object->getEndAt();

        $conflicts = $this->em->getRepository(Booking::class)->findOverlappingWithRange($roomId, $beginAt, $endAt);
        $conflicts = array_column($conflicts, "id");

        if ($object->getId()){
            $key = array_search($object->getId(), $conflicts);
            if ($key!==false){
                unset($conflicts[$key]);
            }
        }

        if (count($conflicts)>0) {
            $this->context->buildViolation($constraint->message)->atPath('beginAt')
                ->addViolation();
        }
    }

}