<?php
/**
 * Created by PhpStorm.
 * User: pandora
 * Date: 08/01/19
 * Time: 16:00
 */

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class AvailableDate extends Constraint
{
    public $message = 'Sorry this room is not available during this time slot.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

}