<?php

namespace App\Repository;

use App\Entity\Booking;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Booking::class);
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function findBookingsByUser($userId)
    {
        return $this->createQueryBuilder('b')
            ->where('b.user = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getResult();
    }

    public function findBookingsByRoom($roomId)
    {
        return $this->createQueryBuilder('b')
            ->where('b.room = :roomId')
            ->setParameter('roomId', $roomId)
            ->getQuery()
            ->getResult();
    }

    public function checkIfRoomIsAvailable($room, $beginAt, $endAt)
    {
        return $this->createQueryBuilder('b')
            ->join('b.room', 'r')
            ->select('b.id')
            ->where('r.name = :room')
            ->setParameter('room', $room)
            ->andWhere('b.beginAt BETWEEN :beginAt and :endAt 
                        OR b.endAt BETWEEN :beginAt and :endAt 
                        OR b.beginAt <= :beginAt AND b.endAt >= :endAt')
            ->setParameter('beginAt', $beginAt)
            ->setParameter('endAt', $endAt)
            ->getQuery()
            ->execute();
    }

    public function findOverlappingWithRange($roomId, $beginAt, $endAt)
    {
        return $this->createQueryBuilder('b')
            ->join('b.room', 'r')
            ->select('b.id')
            ->andWhere('r.id = :roomId')
            ->setParameter('roomId', $roomId)
            ->andWhere('b.beginAt <= :endAt AND b.endAt >= :beginAt')
            ->setParameter('beginAt', $beginAt)
            ->setParameter('endAt', $endAt)
            ->getQuery()
            ->getScalarResult()
            ;
    }
    // /**
    //  * @return Reservation[] Returns an array of Reservation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Reservation
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
