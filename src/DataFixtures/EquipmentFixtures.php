<?php

namespace App\DataFixtures;

use App\Entity\Equipment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class EquipmentFixtures extends Fixture
{
    public const VIDEO_PROJECTOR = 'video-projector';
    public const VIDEO_CONFERENCING_SYSTEM = 'video-conferencing-system';
    public const WHITEBOARD = 'whiteboard';
    public const SCREEN = 'screen';

    public function load(ObjectManager $manager)
    {
        $videoProjector = new Equipment();
        $videoProjector->setName('video projector');
        $this->addReference(self::VIDEO_PROJECTOR, $videoProjector);

        $videoConferencingSystem = new Equipment();
        $videoConferencingSystem->setName('video conferencing system');
        $this->addReference(self::VIDEO_CONFERENCING_SYSTEM, $videoConferencingSystem);

        $whiteboard = new Equipment();
        $whiteboard->setName('whiteboard');
        $this->addReference(self::WHITEBOARD, $whiteboard);

        $screen = new Equipment();
        $screen->setName('screen');
        $this->addReference(self::SCREEN, $screen);

        $manager->persist($videoProjector);
        $manager->persist($videoConferencingSystem);
        $manager->persist($whiteboard);
        $manager->persist($screen);
        $manager->flush();
    }
}
