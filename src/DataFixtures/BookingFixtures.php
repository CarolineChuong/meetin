<?php

namespace App\DataFixtures;

use App\Entity\Booking;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class BookingFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $beginAt1 = new \DateTime();
        $beginAt1->setDate(2019, 01, 10);
        $beginAt1->setTime(10, 00);
        $beginAt1->setTimezone(new \DateTimeZone('Europe/Paris'));

        $endAt1 = new \DateTime();
        $endAt1->setDate(2019, 01, 10);
        $endAt1->setTime(12, 30);
        $endAt1->setTimezone(new \DateTimeZone('Europe/Paris'));

        $booking1 = new Booking();
        $booking1->setTitle('Dev meeting');
        $booking1->setBeginAt($beginAt1);
        $booking1->setEndAt($endAt1);
        $booking1->setUser($this->getReference(UserFixtures::USER_1));
        $booking1->setRoom($this->getReference(RoomFixtures::ROOM_1));

        $beginAt2 = new \DateTime();
        $beginAt2->setDate(2019, 01, 8);
        $beginAt2->setTime(14, 00);
        $beginAt2->setTimezone(new \DateTimeZone('Europe/Paris'));

        $endAt2 = new \DateTime();
        $endAt2->setDate(2019, 01, 8);
        $endAt2->setTime(16, 00);
        $endAt2->setTimezone(new \DateTimeZone('Europe/Paris'));

        $booking2 = new Booking();
        $booking2->setTitle('Board meeting');
        $booking2->setBeginAt($beginAt2);
        $booking2->setEndAt($endAt2);
        $booking2->setUser($this->getReference(UserFixtures::USER_2));
        $booking2->setRoom($this->getReference(RoomFixtures::ROOM_5));

        $beginAt3 = new \DateTime();
        $beginAt3->setDate(2019, 01, 8);
        $beginAt3->setTime(9, 00);
        $beginAt3->setTimezone(new \DateTimeZone('Europe/Paris'));

        $endAt3 = new \DateTime();
        $endAt3->setDate(2019, 01, 8);
        $endAt3->setTime(10, 30);
        $endAt3->setTimezone(new \DateTimeZone('Europe/Paris'));

        $booking3 = new Booking();
        $booking3->setTitle('Interview');
        $booking3->setBeginAt($beginAt3);
        $booking3->setEndAt($endAt3);
        $booking3->setUser($this->getReference(UserFixtures::USER_2));
        $booking3->setRoom($this->getReference(RoomFixtures::ROOM_1));


        $manager->persist($booking1);
        $manager->persist($booking2);
        $manager->persist($booking3);
        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [UserFixtures::class, RoomFixtures::class];
    }
}
