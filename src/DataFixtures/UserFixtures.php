<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserFixtures extends Fixture
{
    public const USER_1 = 'user1';
    public const USER_2 = 'user2';

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $admin->setName('Root');
        $admin->setFirstname('Admin');
        $admin->setEmail('admin@meetin.com');
        $admin->setPassword($this->passwordEncoder->encodePassword($admin,'admin123'));
        $admin->setRoles(['ROLE_ADMIN']);

        $user1 = new User();
        $user1->setName('Tata');
        $user1->setFirstname('Toto');
        $user1->setEmail('toto@meetin.com');
        $user1->setPassword($this->passwordEncoder->encodePassword($user1,'toto1234'));
        $this->addReference(self::USER_1, $user1);

        $user2 = new User();
        $user2->setName('Bobby');
        $user2->setFirstname('Bob');
        $user2->setEmail('bob@meetin.com');
        $user2->setPassword($this->passwordEncoder->encodePassword($user2,'bob12345'));
        $this->addReference(self::USER_2, $user2);

        $manager->persist($admin);
        $manager->persist($user1);
        $manager->persist($user2);

        $faker = Faker\Factory::create('en_US');
        for ($i = 0; $i < 30; $i++){
            $user = new User();
            $user->setName($faker->lastName);
            $user->setFirstname($faker->firstName);
            $user->setEmail(strtolower($user->getFirstname().'.'.$user->getName().'@meetin.com'));
            $user->setPassword($this->passwordEncoder->encodePassword($user,'user1234'));
            $manager->persist($user);
        }
        $manager->flush();
    }
}
