<?php

namespace App\DataFixtures;

use App\Entity\Room;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class RoomFixtures extends Fixture implements DependentFixtureInterface
{
    public const ROOM_1 = 'room1';
    public const ROOM_5 = 'room5';

    public function load(ObjectManager $manager)
    {
        $videoProjector = $this->getReference(EquipmentFixtures::VIDEO_PROJECTOR);
        $videoConferencingSystem = $this->getReference(EquipmentFixtures::VIDEO_CONFERENCING_SYSTEM);
        $whiteboard = $this->getReference(EquipmentFixtures::WHITEBOARD);
        $screen = $this->getReference(EquipmentFixtures::SCREEN);

        $room1 = new Room();
        $room1->setName('room1');
        $room1->setCapacity('6');
        $room1->setImage('room1.jpg');
        $room1->addEquipment($whiteboard);
        $this->addReference(self::ROOM_1, $room1);

        $room2 = new Room();
        $room2->setName('room2');
        $room2->setCapacity('8');
        $room2->setImage('room2.jpg');
        $room2->addEquipment($whiteboard);
        $room2->addEquipment($screen);

        $room3 = new Room();
        $room3->setName('room3');
        $room3->setCapacity('8');
        $room3->setImage('room3.jpg');
        $room3->addEquipment($whiteboard);
        $room3->addEquipment($screen);

        $room4 = new Room();
        $room4->setName('room4');
        $room4->setCapacity('12');
        $room4->setImage('room4.jpg');
        $room4->addEquipment($whiteboard);
        $room4->addEquipment($videoProjector);

        $room5 = new Room();
        $room5->setName('room5');
        $room5->setCapacity('20');
        $room5->setImage('room5.jpg');
        $room5->addEquipment($videoConferencingSystem);
        $room5->addEquipment($videoProjector);
        $this->addReference(self::ROOM_5, $room5);

        $manager->persist($room1);
        $manager->persist($room2);
        $manager->persist($room3);
        $manager->persist($room4);
        $manager->persist($room5);
        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [EquipmentFixtures::class];
    }
}
