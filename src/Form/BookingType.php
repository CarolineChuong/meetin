<?php

namespace App\Form;

use App\Entity\Booking;
use App\Entity\Room;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => 'meeting name'
                ]
            ])
            ->add('beginAt', DateTimeType::class, [
                'widget' => 'single_text',
                'required' => true,
                'html5' => false,
                'attr' => [
                    'placeholder' => 'begining date and time',
                    'class' => 'datetimepicker-input',
                    'target' => '#datetimepicker1'
                ]
            ])
            ->add('endAt', DateTimeType::class,[
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'attr' => [
                    'placeholder' => 'end date and time',
                    'class' => 'datetimepicker-input',
                    'target' => '#datetimepicker2'
                ],
            ])
            ->add('room', EntityType::class,[
                'class' => Room::class
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
        ]);
    }
}
