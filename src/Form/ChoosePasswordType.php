<?php
/**
 * Created by PhpStorm.
 * User: pandora
 * Date: 10/12/18
 * Time: 16:48
 */

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;

class ChoosePasswordType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Enter your email',
                'attr' => [
                    'placeholder' => 'Email',
                    'class' => 'form-control',
                    'style' => "margin-bottom: 10px"
                ]
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Choose a password',
                'constraints' => [new Regex([
                    'pattern'=> '/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/',
                    'match' => true,
                    'message' => 'Your password must have at least 8 characters and contain a number'
                ])],
                'attr' => [
                    'placeholder' => 'Password',
                    'class' => 'form-control',
                    'style' => "margin-bottom: 10px"
                ]
            ])
            ->add('token', HiddenType::class, [
                'data_class' => null,
                'attr' => [
                    'value' => $options['token']
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Save',
                'attr' => [
                    'class' =>"btn btn-lg btn-primary"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'token' =>null
        ]);
    }
}
