<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as MyAssert;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"booking"}},
 *     attributes={"access_control"="is_granted('ROLE_USER')"},
 *     collectionOperations={
 *         "get",
 *         "post"
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={"access_control"="is_granted('ROLE_USER') and object.user == user or is_granted('ROLE_ADMIN')" ,"access_control_message"="You can only update your bookings."},
 *         "delete"={"access_control"="is_granted('ROLE_USER') and object.user == user or is_granted('ROLE_ADMIN')" ,"access_control_message"="You can only delete your bookings."}
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\BookingRepository")
 * @MyAssert\AvailableDate()
 */
class Booking
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("booking")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups("booking")
     */
    private $title;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("booking")
     * @Assert\GreaterThan("now", message="The date and time must start at least now")
     * @MyAssert\Weekday
     * @MyAssert\WorkingHour
     */
    private $beginAt;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("booking")
     * @Assert\GreaterThan(propertyPath="beginAt", message="The end date must be greater than begin date")
     * @MyAssert\Weekday
     * @MyAssert\WorkingHour
     */
    private $endAt;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups("booking")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Room", inversedBy="bookings")
     * @Groups("booking")
     * @ORM\JoinColumn(nullable=false)
     */
    private $room;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     * @Groups("booking")
     */
    public $user;

    public function __construct()
    {
        $this->setStatus('open');
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getBeginAt()
    {
        return $this->beginAt;
    }

    /**
     * @param mixed $beginAt
     */
    public function setBeginAt($beginAt): void
    {
        $this->beginAt = $beginAt;
    }

    /**
     * @return mixed
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * @param mixed $endAt
     */
    public function setEndAt($endAt): void
    {
        $this->endAt = $endAt;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function setRoom(?Room $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

}
