<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\Room;
use App\Form\BookingType;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * Class BookingController
 * @package App\Controller
 * @IsGranted("ROLE_USER")
 */
class BookingController extends AbstractController
{

    /**
     * @Route("/my-bookings", name="booking_my_bookings", methods="GET|POST")
     * @return Response
     */
    public function showBookingsByUser()
    {
        return $this->render('booking/my_bookings.html.twig');
    }

    /**
     * @Route("/room-bookings/{roomId}", name="booking_room_booking")
     * @param int $roomId
     * @return Response
     */
    public function showBookingsByRoom(int $roomId)
    {
        $rooms = $this->getDoctrine()->getRepository(Room::class)
            ->findAll();

        $room = $this->getDoctrine()->getRepository(Room::class)->findOneById($roomId);



        return $this->render('booking/room_bookings.html.twig', [
            'rooms' => $rooms,
            'room' => $room
        ]);
    }

    /**
     * @Route("/new-booking", name="booking_new", methods="GET|POST")
     * @param Request $request
     * @param MailerService $mailer
     * @return Response
     */
    public function new(Request $request, MailerService $mailer): Response
    {
        $booking = new Booking();
        $form = $this->createForm(BookingType::class, $booking);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ($booking->getBeginAt() < $booking->getEndAt()) {

                $bookingValidation = $this->getDoctrine()->getRepository(Booking::class)
                    ->checkIfRoomIsAvailable(
                        $booking->getRoom()->getName(),
                        $booking->getBeginAt(),
                        $booking->getEndAt()
                    );

                if (empty($bookingValidation)) {

                    $this->addFlash('notice', 'Booking is confirmed! An email has been sent to '.$this->getUser()->getEmail());

                    $booking->setUser($this->getUser());
                    $em->persist($booking);
                    $em->flush();

                    $mailer->sendBookingDetails($booking->getUser(), $booking);
                    return $this->redirectToRoute('index');
                } else {
                    $this->addFlash('error', 'Sorry there\'s already a booking on this time slot ');
                }
            } else {
                $this->addFlash('error', 'Please check the meeting dates');
            }
        }

        return $this->render('booking/new.html.twig', [
            'booking' => $booking,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show-booking/{id}", name="booking_show", methods="GET")
     * @param Booking $booking
     * @return Response
     */
    public function show(Booking $booking): Response
    {
        return $this->render('booking/show.html.twig', ['booking' => $booking]);
    }

    /**
     * @Route("/edit-booking/{id}", name="booking_edit", methods="GET|POST")
     * @param Request $request
     * @param Booking $booking
     * @param MailerService $mailer
     * @return Response
     */
    public function edit(Request $request, Booking $booking, MailerService $mailer): Response
    {
        $form = $this->createForm(BookingType::class, $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->flush();

            $mailer->sendBookingUpdate($booking->getUser(), $booking);
            $this->addFlash('notice', 'Booking is updated! An email has been sent to '.$this->getUser()->getEmail());

            return $this->redirectToRoute('booking_show', ['id' => $booking->getId()]);
        }

        return $this->render('booking/edit.html.twig', [
            'booking' => $booking,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="booking_delete", methods="DELETE")
     * @param Request $request
     * @param Booking $booking
     * @param MailerService $mailer
     * @return Response
     */
    public function delete(Request $request, Booking $booking, MailerService $mailer): Response
    {
        if ($this->isCsrfTokenValid('delete'.$booking->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($booking);
            $mailer->sendBookingDelete($booking->getUser(), $booking);
            $em->flush();
        }

        return $this->redirectToRoute('booking_my_bookings');
    }
}
