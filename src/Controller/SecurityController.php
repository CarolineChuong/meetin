<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ChoosePasswordType;
use App\Form\PasswordForgottenType;
use App\Service\MailerService;
use App\Service\TokenGeneratorService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    private $passwordEncoder;
    private $em;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $em)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->em = $em;
    }

    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/confirm-token/{token}", name="security_confirm_token")
     * @param Request $request
     * @param $token
     * @param MailerService $mailer
     * @return Response
     */
    public function confirmToken(Request $request, $token, MailerService $mailer)
    {
        $options = ['token' => $token];

        $form = $this->createForm(ChoosePasswordType::class, [], $options);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            /** @var User $user */
            $user = $this->em->getRepository(User::class)->findOneByEmail($request->get('choose_password')['email']);

            $token = $request->get('choose_password')['token'];
            if (null !== $user){
                if ($this->passwordEncoder->isPasswordValid($user, $token)) {
                    $user->setPassword($this->passwordEncoder->encodePassword($user, $request->get('choose_password')['password'] ));
                    $this->em->flush();

                    $mailer->sendAccountConfirmation($user);
                    $this->addFlash('notice', "Your password has been saved!");


                    return $this->redirectToRoute('app_login');
                } else {
                    $this->addFlash('error', "This link is invalid please follow the link in your email");
                }
            } else {
                $this->addFlash('error',"This email cannot be found");
            }

        }
        return $this->render('security/confirm-token.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/password-forgotten", name="security_password_forgotten")
     * @param Request $request
     * @param MailerService $mailer
     * @param TokenGeneratorService $tokenGenerator
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function passwordForgotten(Request $request, MailerService $mailer, TokenGeneratorService $tokenGenerator, UserPasswordEncoderInterface $passwordEncoder)
    {
        $form = $this->createForm(PasswordForgottenType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            /** @var User $user */
            $user = $this->em->getRepository(User::class)->findOneByEmail($request->get('password_forgotten')['email']);

            if (null!= $user){
                $token = $tokenGenerator->generateToken();
                $url = $tokenGenerator->generateRoute($token);

                $user->setPassword($passwordEncoder->encodePassword($user, $token));
                $this->em->flush();

                $mailer->sendNewToken($user, $url);
                $this->addFlash('notice', 'An email has been sent to '.$request->get('password_forgotten')['email']. ' with a link to choose a new password');
            } else {
                $this->addFlash('error', "Sorry we can't find this email.");
            }
        }
        return $this->render('security/password-forgotten.html.twig', [
            'form' => $form->createView()
        ]);
        }

}
