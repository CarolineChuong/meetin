<?php
/**
 * Created by PhpStorm.
 * User: pandora
 * Date: 05/12/18
 * Time: 11:15
 */

namespace App\Controller;


use App\Entity\Booking;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IndexController
 * @package App\Controller
 * @IsGranted("ROLE_USER")
 */
class IndexController extends AbstractController
{

    /**
     * @Route("/", name="index")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $bookings = $this->getDoctrine()->getRepository(Booking::class)->findAll();
        return $this->render('index/index.html.twig', [
            'bookings' => $bookings
        ]);
    }
}